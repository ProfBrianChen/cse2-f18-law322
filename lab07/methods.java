//Lauren Wasserman
//October 25, 2018
//CS 02 lab 07
//This method will use methods to generate a random sentence
import java.util.Random;
public class method
{
  public static void main(String[] args)
  {
    Random randomGenerator = new Random();
  }
  
  public String adjective(String a)
  {
    int randomInt = randomGenerator.nextInt(10);
    switch(randomInt)
    {
      case 1:
        a = 'lazy';
        break;
      case 2:
        a = 'funny';
        break;
      case 3:
        a = 'clean';
        break;
      case 4:
        a = 'calm';
        break;
      case 5:
        a = 'angry';
        break;
      case 6:
        a = 'nervous';
        break;
      case 7:
        a = 'fat';
        break;
      case 8:
        a = 'tall';
        break;
      case 9:
        a = 'short';
        break;
      case 10:
        a = 'fancy';
        break;
    }
    return a;
  }
  
  public String subjectNouns(String b)
  {
    int randomInt = randomGenerator.nextInt(10);
     switch(randomInt)
    {
      case 1:
        b = 'Joe';
        break;
      case 2:
        b = 'ball';
        break;
      case 3:
        b = 'theatre';
        break;
      case 4:
        b = 'California';
        break;
      case 5:
        b = 'vacation';
        break;
      case 6:
        b = 'sports';
        break;
      case 7:
        b = 'bugs';
        break;
      case 8:
        b = 'birds';
        break;
      case 9:
        b = 'dog';
        break;
      case 10:
        b = 'kid';
        break;
    }
    return b;
  }
  
  public String verb(String c)
  {
    int randomInt = randomGenerator.nextInt(10);
     switch(randomInt)
    {
      case 1:
        c = 'ran';
        break;
      case 2:
        c = 'jumped';
        break;
      case 3:
        c = 'sat';
        break;
      case 4:
        c = 'flew';
        break;
      case 5:
        c = 'walked';
        break;
      case 6:
        c = 'woke';
        break;
      case 7:
        c = 'played';
        break;
      case 8:
        c = 'went';
        break;
      case 9:
        c = 'knew';
        break;
      case 10:
        c = 'wrote';
        break;
    }
    return c;
  }
  
  public String objectNouns(String d)
  {
    int randomInt = randomGenerator.nextInt(10);
     switch(randomInt)
    {
      case 1:
        d = 'you';
        break;
      case 2:
        d = 'me';
        break;
      case 3:
        d = 'him';
        break;
      case 4:
        d = 'her';
        break;
      case 5:
        d = 'it';
        break;
      case 6:
        d = 'us';
        break;
      case 7:
        d = 'them';
        break;
      case 8:
        d = 'they';
        break;
      case 9:
        d = 'my';
        break;
      case 10:
        d = 'we';
        break;
    }
    return d;
  }
}