//Lauren Wasserman
//November 14, 2018
//CSE 002 Hw08
//This program will shuffle a deck of 52 cards
import java.util.Scanner;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;
public class Shuffling
{
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    String[] suitNames = {"C", "H", "S", "D"};
    String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
    String[] cards = new String[52];
    String[] hand = new String[5];
    int numCards = 5;
    int again = 1;
    int index = 51;
    for(int i = 0; i < 52; i++)
    {
      cards[i] = rankNames[i % 13] + suitNames[i/13];
      System.out.print(cards[i] + " ");
    }
    System.out.println();
    printArray(cards);
    shuffle(cards);
    printArray(cards);
    while(again == 1)
    {
      hand = getHand(cards, index, numCards);
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn");
      again = scan.nextInt();
    }
  }
  public static void printArray(String[] cards)
  {
    String[] suitNames = {"C", "H", "S", "D"};
    String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
    String[] hand = new String[5];
    int numCards = 0;
    int again = 1;
    int index = 51;
    for(int k = 0; k < 51; k++)
    {
      cards[k] = rankNames[k%13] + suitNames[k/13];
      System.out.print(cards[k] + " ");
    }
    System.out.println();
  }
  
  public static void shuffle(String[] cards)
  {
    String[] random = new String[52];
    String printing = "";
    Random rnd = ThreadLocalRandom.current();
    for (int i = cards.length - 1; i > 0; i--)
    {
      int index = rnd.nextInt(i + 1);
      String cardX = cards[index];
      cards[index] = cards[i];
      cards[i] = cardX;
      random = cards;
      printing = Arrays.toString(random);
    }
    System.out.println(random);
    System.out.println();
  }
  
  public static void getHand(String[] cards, int index, int numCards)
  {
    String[] hand = new String[5];
    String[] random = cards;
    random = cards;
    for(int n = 0; n < 5; n++)
    {
      System.out.print(random[n] + " ");
    }
    System.out.println();
  }
}