//Lauren Wasserman
//September 16, 2018
//CSE 02 hw 03
//This program will give the acres of land affected by hurricane precipitation and how many inches of rain were dropped on average
//import java.util.Scanner;
import java.util.Scanner;
public class Convert
{
  public static void main(String[] args)
  {
    Scanner myScanner = new Scanner(System.in); //declaring the scanner object by calling the scanner constructor
    System.out.print("Enter the affected area in acres: "); //asks the user for input
    double acres = myScanner.nextDouble(); //accepts the input of how many acres were affected
    System.out.print("Enter the rainfall in the affected area: "); //asks the user for input
    double inchesOfRain = myScanner.nextDouble(); //accepts user input on inches of rain
    double acreInch = acres * inchesOfRain; //calculates the acre inches
    double totalMiles = acreInch * 2.4660669E-8; //calculates the cubic miles
    System.out.println(totalMiles + " cubic miles"); //prints the total cubic miles
  }
} 

