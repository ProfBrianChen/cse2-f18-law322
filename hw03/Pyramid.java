//Lauren Wasserman
//September 16, 2018
//CSE 02 hw 3
//This program will calculate the volume of a pyramid
import java.util.Scanner;
public class Pyramid
{
  public static void main(String[] args)
  {
    Scanner myScanner = new Scanner(System.in); //declaring the scanner object by calling the scanner constructor
    System.out.print("The square side of the pyramid is: "); //user inputs information
    double side = myScanner.nextDouble(); //the length of the side of the pyramid
    System.out.print("The height of the pyramid is: "); //user inputs information
    double height = myScanner.nextDouble(); //the height of the pyramid
    double volume = ((side * side) * height) /3; //calculates the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + volume); //prints the volume of the pyramid
  }
}