//Lauren Wasserman
//September 6, 2018
//CSE 02 Lab02
//Program will print the number of minutes per trip, 
//print the number of counts per trip, print the distance of each trip in miles, and print the distance of the two trips combined
public class Cyclometer
{
  public static void main(String[] args)
  {
    //input data
    int secsTrip1 = 480; //the number of seconds in trip 1
    int secsTrip2 = 3220; //the number of seconds in trip 2
    int countsTrip1 = 1561; //the number of bike rotations in trip 1
    int countsTrip2 = 9037; //the number of bike rotations in trip 2
    double wheelDiameter = 27.0; //the bike wheel diameter
    double PI = 3.14159; //the number for PI
  	int feetPerMile = 5280; //how many feet are in a mile
  	int inchesPerFoot = 12; //how many inches are in a foot
  	int secondsPerMinute = 60; //how many seconds are in a minute
	  double distanceTrip1; //variable for the distance of trip 1
    double distanceTrip2; //variable for the distance of trip 2
    double totalDistance; //variable for the total distance(trip1 + trip2)
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts");
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts");
    
    distanceTrip1 = (countsTrip1*wheelDiameter*PI); //will give the distance in inches
	  distanceTrip1/=inchesPerFoot*feetPerMile; //will give distance in miles
	  distanceTrip2 = (countsTrip2*wheelDiameter*PI); //will give distance in inches
    distanceTrip2 /= inchesPerFoot*feetPerMile; //will give distance in miles
	  totalDistance = distanceTrip1 + distanceTrip2; //the total distance in miles
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	  System.out.println("The total distance was " + totalDistance + " miles");



  }
}