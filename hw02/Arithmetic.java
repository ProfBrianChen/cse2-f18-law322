//Lauren Wasserman
//September 9, 2018
//CSE 02 Homework 2
//This program will calculate the total cost of items in a gorcery store plus tax
public class Arithmetic
{
  public static void main(String[] args)
  {
    int numPants = 3; //Number of pairs of pants
    double pantPrice = 34.98; //Cost per pair of pants
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
    int numBelts = 1; //Number of belts
    double beltPrice = 33.99; //cost per belt
    double paSalesTax = 0.06; //the tax rate
    double totalCostOfPants; //total cost of pants
    double pantsSalesTax; //the sales tax for pants
    double totalCostOfShirts; //total cost of shirts
    double shirtsSalesTax; //the sales tax for pants
    double totalCostOfBelts; //total cost of belts
    double beltsSalesTax; //the sales tax for belts
    double purchases; //the total of purchases before sales tax
    double tax; //the total sales tax
    double finalPurchases; //the total including sales tax
    
    totalCostOfPants = numPants * pantPrice; //computes the total price of pants
    System.out.println("The total cost of pants = " + totalCostOfPants); //prints the total price of pants
    double x = totalCostOfPants * paSalesTax; //computes the sales tax for pants
    x = x * 100;
    int a = (int)x;
    pantsSalesTax = a/100.0;
    System.out.println("The sales tax for pants = " + pantsSalesTax); //prints the sales tax for pants
    
    totalCostOfShirts = numShirts * shirtPrice; //computes the total price of shirts
    System.out.println("The total cost of shirts = " + totalCostOfShirts); //prints the total price of shirts
    double y = totalCostOfShirts * paSalesTax; //computes the sales tax for shirts
    y = y * 100;
    int b = (int)y;
    shirtsSalesTax = b/100.0;
    System.out.println("The sales tax for pants = " + shirtsSalesTax); //prints the sales tax for shirts
    
    totalCostOfBelts = numBelts * beltPrice; //computes the total cost of belts
    System.out.println("The total cost of belts = " + totalCostOfBelts); //prints the toal cost of belts
    double z = totalCostOfBelts * paSalesTax; //computes the sales tax for belts
    z = z * 100;
    int c = (int)z;
    beltsSalesTax = c/100.0;
    System.out.println("The sales tax for belts = " + beltsSalesTax); //prints the sales tax for belts
    
    purchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //computes the total of purchases before sales tax
    System.out.println("The total before sales tax = " + purchases); //prints the total before sales tax
    
    tax = pantsSalesTax + shirtsSalesTax + beltsSalesTax; //computes the total sales tax
    System.out.println("The total sales tax = " + tax); //prints the total sales tax
    
    finalPurchases = (totalCostOfPants + pantsSalesTax) + (totalCostOfShirts + shirtsSalesTax) + (totalCostOfBelts + beltsSalesTax); //prints the total with sales tax
    System.out.println("The total after sales tax = " + finalPurchases); //prints the total after sales tax
  }
}