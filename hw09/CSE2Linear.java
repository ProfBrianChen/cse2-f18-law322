//Lauren Wasserman
//November 27, 2018
//CSE 002 Hw 09 part 1
//This program will ask a user for final grades and check to make sure the input is correct
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
public class CSE2Linear
{
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    int[] list = new int[15];
    for(int k = 0; k < list.length; k++)
    {
      System.out.print("Please enter a final grade: ");
      list[k] = scan.nextInt();
      if(scan.hasNextInt())
      {
        if(list[k] < 0 || list[k] > 100)
        {
          System.out.println("Error out of range!");
          System.out.print("Please enter a different grade: ");
          list[k] = scan.nextInt();
        }
        else if(list[k] < list[k-1])
        {
          System.out.println("Error invalid entry!");
          System.out.print("Please enter a different grade: ");
          list[k] = scan.nextInt();
        }
        else
        {
          System.out.print("Please enter a final grade: ");
        }
      }
      else
      {
        System.out.println("Error not an int!");
        System.out.print("Please a different grade: ");
        list[k] = scan.nextInt();
      }
    }
    System.out.println("List of grades " + Arrays.toString(list));
    System.out.print("Enter a grade to be searched for: ");
    int grade = scan.nextInt();
    System.out.println(binary(list, grade));
    scramble(list);
    System.out.print("Enter another grade to be searched for: ");
    int linearGrade = scan.nextInt();
    System.out.println(linear(list, linearGrade));
  }
  
  public static String binary(int[] list, int grade)
  {
    int mid;
    int low = 0;
    int high = (list.length - 1);
    int num = 1;
    while(high >= low)
    {
      mid = (high + low) / 2;
      if(list[mid] > grade)
      {
        low = mid + 1;
        num++;
      }
      else if(list[mid] > grade)
      {
        high = mid - 1;
        num++;
      }
      else
      {
        return "The value found at index: " + mid + " using " + num + " iterations";
      }
    }
    return "The grade was not found";
  }
  
  public static void scramble(int[] list)
  {
    int[] random = new int[15];
    String printingRandom = "";
    Random r = ThreadLocalRandom.current();
    for(int i = list.length - 1; i > 0; i--)
    {
      int index = r.nextInt(i + 1);
      int x = list[index];
      list[index] = list[i];
      list[i] = x;
      random = list;
      printingRandom = Arrays.toString(random);
    }
    System.out.println("The shuffled list of grades is: " + printingRandom);
  }
  
  public static String linear(int[] list, int linearGrade)
  {
    int y;
    int count = 0;
    for(int m = 0; m < list.length; m++)
    {
      if(list[m] == linearGrade)
      {
        return "The grade was found at index: " + m + " using " + count + " iterations";
      }
      else
      {
        count++;
      }
    }
    return "The grade was not found";
  }
}