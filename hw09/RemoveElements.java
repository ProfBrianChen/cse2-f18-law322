//Lauren Wasserman
//November 27, 2018
//CSE 002 Hw 09 part 2
//This program will create an array of random ints and delete one member specificed by the user
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class RemoveElements
{
  public static void main(String args[])
  {
    Scanner scan = new Scanner(System.in);
    int num[] = new int[10];
    int newArray1[];
    int newArray2[];
    int index, target;
    String answer = "";
    do
    {
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is: ";
      out += listArray(num);
      System.out.println(out);
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num, index);
      String out1 = "The output array is ";
      out1 += listArray(newArray1);
      System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num, target);
      String out2 = "The output array is ";
      out2 += listArray(newArray2);
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer = scan.next();
    }
    while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[])
  {
    String out = "{";
    for(int j = 0; j < num.length; j++)
    {
      if(j > 0)
      {
        out += " , ";
      }
      out += num[j];
    }
    out += "} ";
    return out;
  }
  
  public static int[] randomInput()
  {
    int[] random = new int[10];
    for(int k = 0; k < random.length; k++)
    {
      random[k] = (int)(Math.random() * 10 + 1);
    }
    System.out.println(Arrays.toString(random));
    return random;
  }
  
  public static int[] delete(int[] list, int pos)
  {
    if(pos < 0 || pos >= list.length)
    {
      return list;
    }
    int[] second = new int[list.length - 1];
    for(int n = 0, m = 0; n < list.length; n++)
    {
      if(n == pos)
      {
        continue;
      }
      second[m++] = list[n];
    }
    return second;
  }
  
  public static int[] remove(int[] list, int target)
  {
    for(int s: list)
    {
      if(s == target)
      {
        System.out.println("Element " + target + " has been found");
        break;
      }
      else
      {
        System.out.println("Element " + target + " was not found");
        break;
      }
    }
    int x = 0;
    for(int i = 0; i < list.length; i++)
    {
      if(list[i] != target)
      {
        list[x++] = list[i];
      }
    }
    return Arrays.copyOf(list, x);
  }
}