//Lauren Wasserman
//September 13, 2018
//CSE 02 Lab 03
//This program will get the original cost of the check at a restaurant, the percentage tip they wish to pay, 
//the number of ways the check will be split, and how much each person in the group needs to spend in order to pay the check.
import java.util.Scanner;
public class Check
{
  public static void main(String[] args)
  {
    Scanner myScanner = new Scanner(System.in); //declaring the scanner object by calling the scanner constructor
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompts user for cost of check
    double checkCost = myScanner.nextDouble(); //checkCost is the amount the check is worth
    System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx: "); //prompts user for tip money
    double tipPercent = myScanner.nextDouble(); //tipPercent is the tip the user inputs
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //prompts user for number of people who were at dinner
    int numPeople = myScanner.nextInt(); //numPeople are the number of people who went out
    double totalCost; //the total cost of dinner
    double costPerPerson; //the total cost each person will pay
    int dollars; //whole dollar amount of cost 
    int dimes, pennies; //for storing digits to the right of the decimal point for the cost$ 
    totalCost = checkCost * (1 + tipPercent); //calculates the total cost of dinner
    costPerPerson = totalCost / numPeople; //calculates the cost per person
    dollars = (int)costPerPerson; //gets the whole amount instead of decimal
    dimes = (int)(costPerPerson * 10) % 10; //gets the first decimal place
    pennies = (int)(costPerPerson * 100) % 10; //gets the second decimal place 
    System.out.println("Each person in the group owes $ " + dollars + '.' + dimes + pennies);
  }
}