//Lauren Wasserman
//October 8, 2018
//CSE 02 hw 05
//This program will randomly pick 5 cards out of a deck and check to see if theyre in a poker suit
import java.util.Scanner;
public class Hw05
{
  public static void main(String[] args)
  {
    Scanner s = new Scanner(System.in);
    String junk;
    int fourOfaKind = 0;
    int threeOfaKind = 0;
    int twoPair = 0;
    int onePair = 0;
    System.out.print("The number of loops: ");
    boolean loopS = s.hasNextInt();
    while(loopS == false)
    {
      junk = s.next();
      System.out.print("The number of loops: ");
      loopS = s.hasNextInt();
    }
    int loops = s.nextInt();
    for(int k = 0; k <= loops; k++)
    {
      int cardOne = (int) (Math.random() * (52)) + 1;
      int cardTwo = (int) (Math.random() * (52)) + 1;
      while(cardOne == cardTwo)
        cardTwo = (int)(Math.random() * (52)) + 1;
      int cardThree = (int) (Math.random() * (52)) + 1;
      while(cardTwo == cardThree)
        cardThree = (int)(Math.random() * (52)) + 1;
      int cardFour = (int) (Math.random() * (52)) + 1;
      while(cardThree == cardFour)
        cardFour = (int)(Math.random() * (52)) + 1;
      int cardFive = (int) (Math.random() * (52)) + 1;
      while(cardFour == cardFive)
        cardFive = (int)(Math.random() * (52)) + 1;
      
    //start of four of a kind
    if(cardOne % cardTwo == cardOne)
    {
      if(cardTwo % cardThree == cardTwo)
      {
        if(cardThree % cardFour == cardThree)
        {
          if(cardFour % cardFive == cardFive)
          {
            if(cardFive % cardFour == cardFive)
            {
              break;
            }
          }
        }
      }
    }
    else
    {
      int a = 0;
      if(cardOne % cardTwo != cardOne)
      {
        a = a + 1;
      }
      else if(cardTwo % cardThree != cardTwo)
      {
        a = a + 1;
      }
      if(cardThree % cardFour == cardThree)
      {
         if(cardFour % cardFive == cardFour)
         {
           if( a == 1)
           {
             fourOfaKind = fourOfaKind + 1;
           }
         }
      }
      else if(cardThree % cardFour != cardThree)
      {
        a = a + 1;
        if(cardFour % cardFive == cardFour)
        {
          if(a == 1)
          {
            fourOfaKind = fourOfaKind + 1;
          }
        }
      }
      else if(cardFour % cardFive != cardFour)
      {
        a = a + 1;
        if(a == 1)
        {
          fourOfaKind = fourOfaKind + 1;
        }
      }
    }
    
   //start of threepair   
    if(cardOne % cardTwo == cardOne)
    {
      if(cardTwo % cardThree == cardTwo)
      {
        if(cardThree % cardFour == cardThree)
        {
           break;
        }
      }
    }
    else
    {
      int b = 0;
      if(cardOne % cardTwo != cardOne)
      {
        b = b + 1;
      }
      else if(cardTwo % cardThree != cardTwo)
      {
        b = b + 1;
      }
      if(cardThree % cardFour == cardThree)
      {
         if(cardFour % cardFive == cardFour)
         {
           if( b == 2)
           {
             threeOfaKind = threeOfaKind + 1;
           }
         }
      }
      else if(cardThree % cardFour != cardThree)
      {
        b = b + 1;
        if(cardFour % cardFive == cardFour)
        {
          if(b == 2)
          {
            threeOfaKind = threeOfaKind + 1;
          }
        }
      }
      else if(cardFour % cardFive != cardFour)
      {
        b = b + 1;
        if(b == 2)
        {
          threeOfaKind = threeOfaKind + 1;
        }
      }
    }
      
    //start of two pair
    if(cardOne % cardTwo == cardOne)
    {
      if(cardTwo % cardThree == cardTwo)
      {
        if(cardThree % cardFour == cardThree)
        {
          break;
        }
      }
    }
    else
    {
      int c = 0;
      if(cardOne % cardTwo != cardOne)
      {
        c = c + 1;
        if(cardTwo % cardThree == cardTwo)
        {
          if(cardThree % cardFour == cardThree)
          {
            if(cardFour % cardFive == cardFour)
            {
              if(c == 1)
              {
                twoPair = twoPair + 1;
              }
            }
          }
        }
      }
      else if(cardTwo % cardThree != cardTwo)
      {
        c = c + 1;
        if(cardThree % cardFour == cardThree)
        {
          if(cardFour % cardFive == cardFour)
          {
            if(c == 1)
            {
              twoPair = twoPair + 1;
            }
          }
        }
      }
      else if(cardThree % cardFour != cardThree)
      {
        c = c + 1;
        if(cardFour % cardFive == cardFour)
        {
          if(c == 1)
          {
            twoPair = twoPair + 1;
          }
        }
      }
      else if(cardFour % cardFive != cardFour)
      {
        c = c + 1;
        if(c == 1)
        {
          twoPair = twoPair + 1;
        }
      }
    }
    
    //start of one pair
    if(cardOne % cardTwo == cardOne)
    {
      if(cardTwo % cardThree == cardTwo)
      {
        if(cardThree % cardFour == cardThree)
        {
          break;
        }
      }
    }
    else
    {
      int d = 0;
      if(cardOne % cardTwo != cardOne)
      {
        d = d + 1;
        if(cardTwo % cardThree == cardTwo)
        {
          if(cardThree % cardFour == cardThree)
          {
            if(cardFour % cardFive == cardFour)
            {
              break;
            }
          }
        }
      }
      else if(cardTwo % cardThree != cardTwo)
      {
        d = d + 1;
        if(cardThree % cardFour == cardThree)
        {
          if(cardFour % cardFive == cardFour)
          {
            if(d == 3)
            {
              onePair = onePair + 1;
            }
          }
        }
      }
      else if(cardThree % cardFour != cardThree)
      {
        d = d + 1;
        if(cardFour % cardFive == cardFour)
        {
          if(d == 3)
          {
            onePair = onePair + 1;
          }
        }
      }
      else if(cardFour % cardFive != cardFour)
      {
        d = d + 1;
        if(d == 3)
        {
          onePair = onePair + 1;
        }
      }
    }
  }
    double probFour = fourOfaKind/loops;
    double probThree = threeOfaKind/loops;
    double probTwo = twoPair/loops;
    double probOne = onePair/loops;
    System.out.printf("The Probability of Four-of-a-Kind: " + "%.3f", probFour);
    System.out.println();
    System.out.printf("The Probability of Three-of-a-Kind: " + "%.3f", probThree);
    System.out.println();
    System.out.printf("The Probability of Two-Pair: " + "%.3f", probTwo);
    System.out.println();
    System.out.printf("The Probability of One-Pair: " + "%.3f", probOne);
    System.out.println();
}
}