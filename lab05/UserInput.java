//Lauren Wasserman
//October 4, 2018
//This code will ask the user for inputs and see if they entered the correct answers
import java.util.Scanner;
public class UserInput
{
  public static void main(String[] args)
  {
    String junk;
    Scanner myScanner = new Scanner(System.in); //declaring the scanner object by calling the scanner constructor
    System.out.print("What's the course number? ");
    boolean num = myScanner.hasNextInt(); //takes the input as a boolean and checks if its an int
    while(num == false) //if its not an int
    {
      junk = myScanner.next(); //stores it as a string
      System.out.print("What's the course number? "); //asks the question again
      num = myScanner.hasNextInt(); //checks again to see if its an int
    }
    int num2 = myScanner.nextInt(); //since it is an int it now stores it
    
    System.out.print("What's the department name? ");
    boolean name = myScanner.hasNextInt(); //takes the input as a boolean and checks if its an int
    while(name == true) //if it is an int then its wrong because the question asks for a string
    {
      junk = myScanner.next(); //stores it as a random string
      System.out.print("What's the department name? "); //asks the question again
      name = myScanner.hasNextInt(); //checks again if its an int
    }
    String name2 = myScanner.next(); //stores it as a string
    
    System.out.print("How many times does the class meet per week? ");
    boolean x = myScanner.hasNextInt(); //takes the input as a boolean and checks if its an int
    while(x == false) //if its not an int
    {
      junk = myScanner.next(); //stores it as a random string
      System.out.print("How many times does the class meet per week? "); //asks the question again
      x = myScanner.hasNextInt(); //checks again to see if its an int
    }
    int x2 = myScanner.nextInt(); //stores it an int
    
    System.out.print("What time does the class start? *input in 2400 hour time* ");
    boolean time = myScanner.hasNextInt(); //takes the input as a boolean and checks if its an int
    while(time == false) //if its not an int
    {
      junk = myScanner.next(); //stores it as a random string
      System.out.print("What time does the class start? "); //asks the question again
      time = myScanner.hasNextInt(); //checks again to see if its an int
    }
    int time2 = myScanner.nextInt();
    myScanner.next();
    
    System.out.print("What's your instructor's name? ");
    boolean prof = myScanner.hasNextInt(); //takes the input as a boolean and checks if its an int
    while(prof == true) //if it is an int
    {
      junk = myScanner.next(); //stores it as a random string
      System.out.print("What's your instructor's name? "); //asks the question again
      prof = myScanner.hasNextInt(); //checks if its an int again
    }
    String prof2 = myScanner.next(); //stores it as a string
    
    System.out.print("How many students are in the class? ");
    boolean student = myScanner.hasNextInt(); //takes the input as a boolean and checks if its an int
    while(student == false) //if its not an int
    {
      junk = myScanner.next(); //stores it as a random string
      System.out.print("How many students are in the class? "); //asks the question again
      student = myScanner.hasNextInt(); //checks again if its an int
    }
    int student2 = myScanner.nextInt(); //stores the input as an int
  }
}