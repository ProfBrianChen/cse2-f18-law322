//Lauren Wasserman
//11/8/18
//CSE 002lab 08
//This lab will create two arrays, one with random numbers and the other will count how many of each number are in the first array
public class arrays
{
  public static void main(String[] args)
  {
    int count = 0;
    int num = (int) (Math.random() * 99 + 1);
    int[] firstArray = new int[101];
    int[] secondArray = new int[200];
    for(int k = 0; k <= 100; k++)
    {
      firstArray[k] = num;
       num = (int) (Math.random() * 99 + 1);
    }
    int length = firstArray.length;
    for(int k = 0; k <100; k++)
    {
      for(int j = 0; j <length; j++)
      {
        if(firstArray[j] == k)
        {
          count++;
          secondArray[j] = count;
        }
      }
      System.out.println(k + " occurs " + count + " times");
      count = 0;
    }
  }
}