//Lauren Wasserman
//October 11, 2018
//CSE 02 lab 06
//This program uses nested loops to display a pattern
import java.util.Scanner;
public class PatternC
{
  public static void main(String[] args)
  {
    String junk;
    Scanner s = new Scanner(System.in);
    System.out.print("Enter an integer between 1 and 10 ");
    boolean x = s.hasNextInt(); //takes the input as a boolean and checks if its an int
    while(!x)
    {
      junk = s.next(); //stores it as a random string
      System.out.print("Enter an integer between 1 and 10 "); //asks the question again
      x = s.hasNextInt(); //checks again to see if its an int
    }
    int number = s.nextInt(); //stores it an int
    if(number > 10 || number < 1)
    {
     x = false;
    }
     else
     {
        x = true;
     }
    
    for(int numRows = 1; numRows <= number; numRows++)
    {
      for(int j = number-1; j >= numRows; j--)
      {
        System.out.print(" ");
      }
      for(int j = numRows; j >= 1; j--)
      {
        System.out.print(j);
      }
      System.out.println();
    }
  }
}