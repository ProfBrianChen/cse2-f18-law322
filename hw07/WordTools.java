//Lauren Wasserman
//October 28, 2018
//CSE 002 Hw 07
//This program will use a variety of methods to analyze and print strings

import java.util.Scanner;
public class WordTools
{
  public static void main(String args[])
  {
    String text = sampleText();
    System.out.println("You entered: " + text);
    printMenu(text);
    
  }
  public static String sampleText()
  {
    Scanner s = new Scanner(System.in);
    System.out.print("Enter a sample text: ");
    String text = s.nextLine();
    return text;
  }
  
  public static String printMenu(String sample)
  {
    String x = sample;
    Scanner s = new Scanner(System.in);
    System.out.println("MENU");
    System.out.println("c - number of non-whitespace characters");
    System.out.println("w - number of words");
    System.out.println("f - find text");
    System.out.println("r - replace all !'s");
    System.out.println("s - shorten spaces");
    System.out.println("q - quit");
    System.out.println("Choose an option: ");
    String option = s.nextLine();
    while(true)
    {
      if(option.equals("c"))
      {
        System.out.println("Number of non-white characters: " + getNumOfNonWSCharacters(sample));
        System.out.println();
        return printMenu(x);
      }
      else if(option.equals("w"))
      {
        System.out.println("You had " + getNumOfWords(sample) + " words");
        System.out.println();
        return printMenu(x);
      }
      else if(option.equals("f"))
      {
        String t = "";
        System.out.println("Your word appeard " + findText(t, sample) + " times");
        System.out.println();
        return printMenu(x);
      }
      else if(option.equals("r"))
      {
        System.out.println("Your new string looks like " + replaceExlamation(sample));
        System.out.println();
        return printMenu(x);
      }
      else if(option.equals("s"))
      {
        System.out.println("Your new string looks like: " + shortenSpace(sample));
        System.out.println();
        return printMenu(x);
      }
      else if(option.equals("q"))
      {
        return "You picked to quit";
      }
      else
      {
        System.out.println("You entered an invalid character");
        System.out.println();
        return printMenu(x);
      }
    }
  }
  
  public static int getNumOfNonWSCharacters(String num)
  {
    String newString = num.replaceAll("\\s+", " ");
    int length = newString.length();
    return length;
  }
  
  public static int getNumOfWords(String words)
  {
//     String[] number = words.split(" ");
//     return number.length;
    int count = 0;
    for(int i = 0; i < words.length(); i++){
      if(words.charAt(i) == ' '){
        count++;
      }
    }
    return count+1;
  }
  
  public static int findText(String x, String text) //first parameter = text to be found, second paramenter = text the user provides
  {
    int total = 0;
    System.out.print("Enter a word or phrase to be found: ");
    Scanner s = new Scanner(System.in);
    text = s.next();
    if(x.matches("quit"))
    {
      return total;
    }
    String y[] = text.split(" ");
    for(int k = 0; k < y.length; k++)
    {
      if(y[k].equals(text))
      {
        total = total + 1;
      }
    }
    return total;
  }
  
  public static String replaceExlamation(String sam)
  {
    int z = sam.length();
    StringBuilder a = new StringBuilder(z);
    for(int k = 0; k < z; k++)
    {
      a.append('*');
    }
    return a.toString();
  }
  
  public static String shortenSpace(String sample)
  {
    String shortenedSpaces = sample.replaceAll("\\s{2,}", " ");
    return shortenedSpaces;
  }
}