//Lauren Wasserman
//October 28, 2018
//CSE 002 Hw 07
//This program will use a variety of methods to analyze and print strings

import java.util.Scanner;
public class wordTools
{
  public static void main(String args[])
  {
    String text = sampleText();
    System.out.println("You entered: " + text);
    System.out.println(printMenu(text));
    
  }
  public static String sampleText()
  {
    Scanner s = new Scanner(System.in);
    System.out.println("Enter a sample text: ");
    String text = s.nextLine();
    return text;
  }
  
  public static String printMenu(String sample)
  {
    String y = "";
    Scanner s = new Scanner(System.in);
    System.out.println("MENU");
    System.out.println("c - number of non-whitespace characters");
    System.out.println("w - number of words");
    System.out.println("f - find text");
    System.out.println("r - replace all !'s");
    System.out.println("s - shorten spaces");
    System.out.println("q - quit");
    System.out.println("Choose an option: ");
    String option = s.nextLine();
    while(true)
    {
      if(option.equals("c"))
      {
        System.out.println("Number of non-while characters: " + getNumOfNonWSCharacters(option));
      }
      else if(option.equals("w"))
      {
        System.out.println("You had " + getNumOfWords(option) + "words");
      }
      else if(option.equals("f"))
      {
        String t = "";
        System.out.println("Your word appeard " + findText(t, option) + " times");
      }
      else if(option.equals("r"))
      {
        System.out.println("Your new string looks like " + shortenSpace(option));
      }
      else if(option.equals("s"))
      {
        System.out.println("Your new string looks like: " + shortenSpace(option));
      }
      else if(option.equals("q"))
      {
        System.out.println("You picked to quit");
      }
      else
      {
        System.out.println("You entered an invalid character");
      }
    }
  }
  
  public static int getNumOfNonWSCharacters(String num)
  {
    String newString = num.replaceAll("\\s+", " ");
    int length = newString.length();
    return length;
  }
  
  public static int getNumOfWords(String words)
  {
    String[] number = words.split(" ");
    return number.length;
  }
  
  public static int findText(String x, String text) //first parameter = text to be found, second paramenter = text the user provides
  {
    int total = 0;
    String finder = "";
    Scanner s = new Scanner(System.in);
    finder = s.next();
    if(x.matches("quit"))
    {
      return total;
    }
    String y[] = text.split(" ");
    for(int k = 0; k < y.length; k++)
    {
      if(y[k].equals(finder))
      {
        total = total + 1;
      }
    }
    return total;
  }
  
  public static String replaceExlamation(String sam)
  {
    int z = sam.length();
    StringBuilder a = new StringBuilder(z);
    for(int k = 0; k < z; k++)
    {
      a.append('*');
    }
    return a.toString();
  }
  
  public static String shortenSpace(String sample)
  {
    String shortenedSpaces = sample.replaceAll("\\s{2,}", " ");
    return shortenedSpaces;
  }
}