//Lauren Wasserman
//October 21, 2018
//CSE 02 Hw06
//This program will take a user input and print a box with an encyrpted x inside it
import java.util.Scanner;
public class EncryptedX
{
  public static void main(String[] args)
  {
    Scanner s = new Scanner(System.in); //instantiates the scanner object
    int number = 0; //declares the variable number that will store the correct user input
    System.out.print("Input a number between 0 and 100: "); //asks the question
    int y = s.nextInt(); //stores the answer as an int
    number = y;
    if(y < 0 || y > 100) //sees if its out of range
    {
      System.out.print("Input a number between 0 and 100: "); //asks the question again
      number = s.nextInt(); //stores the new input as the int number
    }
    Box(number); //calls the method that prints the box
  }
  
  public static void Box(int x)
  {
     for(int k = 0; k <= x; k++) //a for loop that runs through the user input
     {
       for(int j = 0; j < x + 1; j++) //a for loop that goes through each horizontal line of the box
       {
         if(j == k) //if j is equal to the number of the row then it prints a blank space
         {
            System.out.print(" ");
         }
         else if(j == (x - k)) //if its equal to the number of the row on the other side of the box then it prints a blank space
         {
           System.out.print(" ");
         }
         else //while going through this row in the for loop if it doesnt equal anything special it prints the *
         {
            System.out.print("*");
         }
       }
       System.out.println(); //new line
     }
   }
}