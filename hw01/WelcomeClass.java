public class WelcomeClass
{
  public static void main(String args[])
  {
    System.out.println("-----------");
    System.out.println("| Welcome |");
    System.out.println("-----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-8--9--2--3--4--3->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
}